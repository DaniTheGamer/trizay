﻿using UnityEngine;
using UnityEngine.Networking;

//This is the user for the smartboard, this entity should keep track of things such as the scoreboard
public class UserSmartboard : User {
    private FollowCamera cmF;

    private void Start() {
        cmF = Camera.main.GetComponent<FollowCamera>();
        cmF.UpdateTarget(gameObject);
    }

    public FollowCamera GetCamera() {
        return cmF;
    }

    [ClientRpc]
    public void RpcEnableSmartboard() {
        
    }
}