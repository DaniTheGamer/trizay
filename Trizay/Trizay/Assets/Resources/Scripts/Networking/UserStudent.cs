﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

//This is the user created if you're student playing
public class UserStudent : User {
    [SerializeField] private GameObject prefabPlayer;
    private User parent;
    private GameObject playerSpawned;

    [SerializeField] private int scoreGainCorrect;
    [SerializeField] private float scoreGainStreakMult;
    [SerializeField] private float scoreDefaultStreakMult;

    /*[SyncVar]*/ private int score = 0;
    /*[SyncVar]*/ private float scoreStreakMult;

    // Start is called before the first frame update
    void Start() {
        scoreStreakMult = scoreDefaultStreakMult;
        if (transform.parent != null) {
            parent = transform.parent.GetComponent<User>(); 
        } else {
            parent = this;
        }

        if (!parent.hasAuthority) {
            return;
        }

        CmdSpawnPlayer();
    }
 
    IEnumerator WaitForReady() {
        while (parent.connectionToClient == null) {
            Debug.Log("Waiting for client to be ready - Connection");
            yield return new WaitForSeconds(0.25f);
        }

        while (!parent.connectionToClient.isReady) {
            Debug.Log("Waiting for client to be ready - Connection");
            yield return new WaitForSeconds(0.25f);
        }
        Spawn();
    }

    ////////////Server commands (stuff here only runs on server because either [Server] or [Command] is written before method definition)
    [Command]
    void CmdSpawnPlayer() {
        StartCoroutine(WaitForReady());
    }

    [Server]
    void Spawn() {
        //Find spawn areas and log count
        var SpawnAreas = GameObject.FindGameObjectsWithTag("Spawn Area");
        Debug.Log("Found spawn areas: "+SpawnAreas.Length);

        //Choose random area from spawn areas
        var rd = Random.Range(0, SpawnAreas.Length - 1);
        var area = SpawnAreas[rd].GetComponent<BoxCollider>();

        //Choose random location in area
        var areaVec = new Vector3(Random.Range(-area.size.x, area.size.x), 0, Random.Range(-area.size.z, area.size.z));

        //Spawn player at that location translated by the areas center position
        var go = Instantiate(prefabPlayer, area.transform.position + areaVec, Quaternion.identity);

        //Set the name of the player
        go.name = "Player[" + parent.GetUserName() + "]";

        //Assign color to player
        var c = parent.GetUserColor();
        var rends = go.GetComponentsInChildren<Renderer>();
        foreach (var rend in rends) {
            rend.material.color = new Color(c.r, c.g, c.b, rend.material.color.a); 
        }

        //Spawn the player on the server with authority from the local player
        NetworkServer.SpawnWithClientAuthority(go, parent.connectionToClient);

        //Asks the client to also change its name
        var playerMan = go.GetComponent<PlayerManager>();
        playerMan.RpcChangeName(go.name);
        playerMan.RpcChangeColor(c.r, c.g, c.b);

        playerSpawned = go;

        //States in console that the player of x name has been spawned.
        Debug.Log("Spawned: " + go.name);
    }

    public GameObject GetPlayerSpawnedGO() {
        return playerSpawned;
    }

    [TargetRpc]
    public void TargetUpdateScore(NetworkConnection netCon, int newScore, float newScoreStreakMult) {
        score = newScore;
        scoreStreakMult = newScoreStreakMult;
        GameObject.Find("LobbyManager").transform.Find("UI Cap").transform.Find("Player POV").transform.Find("Panel").Find("Player Score").GetComponent<Text>().text = "" + newScore;
    }

    [Command]
    public void CmdCalculateScore() {
        Debug.Log("Calculating score for: " + transform.parent.name);
        if (transform.parent.name.ToLower().Contains("kage")) { //If it should give points
            score = (int)(score+(scoreGainCorrect*scoreStreakMult));
            scoreStreakMult += scoreGainStreakMult;
            TargetUpdateScore(parent.connectionToClient, score, scoreStreakMult);
        } else {
            scoreStreakMult = scoreDefaultStreakMult;
            TargetUpdateScore(parent.connectionToClient, score, scoreStreakMult);
        }
    }
}