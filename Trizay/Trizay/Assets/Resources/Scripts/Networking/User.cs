﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//This is the user which decides which type of user to spawn
public class User : NetworkBehaviour {
    [SerializeField] private GameObject prefabStudent;
    [SerializeField] private GameObject prefabSmartboard;
    private GameObject currentUser;
    private string userName;
    private Color userColor;
    private bool isSmartboard = false;

    public UserSmartboard SpawnSmartboard() {
        isSmartboard = true;
        Camera.main.GetComponent<FollowCamera>().SetIsSmartboard(true);
        return Instantiate(prefabSmartboard, transform).GetComponent<UserSmartboard>();
    }

    public UserStudent GetStudent() {
        return currentUser.GetComponent<UserStudent>();
    }

    public bool GetIsSmartboard() {
        return isSmartboard;
    }

    public Color GetUserColor() {
        return userColor;
    }

    public void SetUserColor(Color newUserColor) {
        userColor = newUserColor;
    }

    public string GetUserName() {
        return userName;
    }

    public void SpawnStudent() {
        CmdSpawnStudent();
    }

    [Command]
    public void CmdSpawnStudent() {
        var student = Instantiate(prefabStudent, transform);
        student.GetComponent<UserStudent>().userName = userName;
        currentUser = student;
        NetworkServer.SpawnWithClientAuthority(student, connectionToClient);
        //RpcSpawn();
    }

    //[ClientRpc]
    //void RpcSpawn() {
    //    if (NetworkServer.active) return;
    //    var student = Instantiate(prefabStudent, transform);
    //    student.GetComponent<NetworkIdentity>().AssignClientAuthority(connectionToClient);
    //    currentUser = student;
    //}

    [ClientRpc]
    public void RpcChangeUserName(string newUserName) {
        if (!isServer) {
            gameObject.name += " [" + newUserName + "]"; 
        }
        userName = newUserName;
    }
}