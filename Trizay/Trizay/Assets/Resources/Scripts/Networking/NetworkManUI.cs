﻿using UnityEngine;
using UnityEngine.Networking;

//This is what is being called by the UI when it's doing network manageing
public class NetworkManUI : MonoBehaviour {
    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.J)) {
            NetworkLobbyManager.singleton.networkAddress = "localhost";
            NetworkLobbyManager.singleton.networkPort = 7777;
            NetworkLobbyManager.singleton.StartHost();
        }

        if (Input.GetKeyDown(KeyCode.K)) {
            NetworkLobbyManager.singleton.networkAddress = "192.168.87.105";
            NetworkLobbyManager.singleton.networkPort = 7777;
            NetworkLobbyManager.singleton.StartClient();
        }
    }
}
