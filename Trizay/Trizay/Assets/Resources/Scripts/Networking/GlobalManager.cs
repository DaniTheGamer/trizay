﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Prototype.NetworkLobby;
using System.Collections;

//This object stays even on game start..
public class GlobalManager : LobbyHook {
    private UserSmartboard userSmartboard;
    private List<UserStudent> students = new List<UserStudent>();
    
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer) {
        base.OnLobbyServerSceneLoadedForPlayer(manager, lobbyPlayer, gamePlayer);

        StartCoroutine(WaitForReady(gamePlayer, lobbyPlayer));
    }

    IEnumerator WaitForReady(GameObject userGo, GameObject lobbyPlayerGo) {
        while (!userGo.GetComponent<User>().isClient) {
            Debug.Log("Waiting to become client - User");
            yield return new WaitForSeconds(0.25f);
        }

        OnRdy(userGo, lobbyPlayerGo);
    }

    public PlayerManager[] GetPlayerManagers() {
        var pMs = new PlayerManager[students.Count];

        for (int studentIndex = 0; studentIndex < students.Count; studentIndex++) {
            pMs[studentIndex] = students[studentIndex].GetPlayerSpawnedGO().GetComponent<PlayerManager>();
        }

        return pMs;
    }

    public UserStudent[] GetStudents() {
        return students.ToArray();
    }

    public UserSmartboard GetSmartboard() {
        return userSmartboard;
    }

    void OnRdy(GameObject userGo, GameObject lobbyPlayerGo) {
        var lobPlayer = lobbyPlayerGo.GetComponent<LobbyPlayer>();
        var user = userGo.GetComponent<User>();
        
        user.RpcChangeUserName(lobPlayer.playerName);
        userGo.name += " [" + lobPlayer.playerName + "]";
        user.SetUserColor(lobPlayer.playerColor);

        if (userSmartboard == null) {
            userSmartboard = user.SpawnSmartboard();
            userSmartboard.RpcEnableSmartboard();
        } else {
            user.SpawnStudent();
            students.Add(user.GetStudent());
        }
    }
}