﻿using UnityEngine;

//This is a class to simply create a question which can be answered (even works with multiple right answers)
[System.Serializable]
[CreateAssetMenu(fileName ="New Trivia", menuName = "Trizay/Trivia")]
public class Trivia : ScriptableObject {
    public QA[] qAs;
}