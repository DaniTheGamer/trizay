﻿using UnityEngine;

//Creates the Answer struct. An answer knows what it's saying and if it's right
[System.Serializable]
public struct Answer {
    public bool isRight;
    public string text;

    public Answer(bool isRight = false, string text = "") {
        this.isRight = isRight;
        this.text = text;
    }
}