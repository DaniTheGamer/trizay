﻿using UnityEngine;

//This is a class to simply create a question which can be answered (even works with multiple right answers)
[System.Serializable]
[CreateAssetMenu(fileName = "New QA", menuName = "Trizay/QA")]
public class QA : ScriptableObject {
    public string Question;
    public Answer[] Answers = new Answer[4];
}