﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//This is a class to simply create a question which can be answered (even works with multiple right answers, maybe)
public class QAManager : NetworkBehaviour {
    [SerializeField] private Trivia trivia;
    [SerializeField] private float roundLength = 30;
    private float roundLengthLeft = 0;
    private int currentRound = 0;
    private QA currentQA;
    private UserStudent[] players;
    [SerializeField] private GlobalManager globMan;

    private void Start() {
        if (!isServer) {
            return;
        }

        StartCoroutine(WaitForClients());
    }

    IEnumerator WaitForClients() {
        while (true) {
            Debug.Log("Waiting for clients to be ready - Connection");
            players = globMan.GetStudents();

            if (players.Length != 0) {
                break;
            }

            yield return new WaitForSeconds(1f);
        }

        OnGameStart();
    }

    private void FixedUpdate() {
        if (!isServer) {
            return;
        } //Only server runs below this line

        if (roundLengthLeft <= 0) {
            NextRound();
        } else {
            roundLengthLeft -= Time.fixedDeltaTime;
        }
    }

    public void NextRound() {
        OnRoundEnd();
        OnRoundStart();
    }

    void OnGameStart() {
        Debug.Log("Game has started");
        OnRoundStart();
    }

    void OnRoundStart() {
        Debug.Log("Round has started");
        roundLengthLeft = roundLength;
        currentQA = trivia.qAs[currentRound];

        //Debug.LogError("Current QA: " + currentQA);
        //Debug.LogError("Current QA Question: " + currentQA.Question);

        if (currentRound+1 < trivia.qAs.Length) {
            currentRound++;
        } else {
            OnGameEnd();
        }
    }

    void OnRoundEnd() {
        Debug.Log("Round has ended");

        //Calculate new score for each player
        foreach (var player in players) {
            player.CmdCalculateScore();
            Debug.Log(player.name);
        }
    }

    void OnGameEnd() {
        Debug.Log("Game has ended");
    }

    public QA GetCurrentQA() {
        return currentQA;
    }
}