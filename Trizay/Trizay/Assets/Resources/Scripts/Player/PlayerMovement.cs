﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//Handles all movement on player (walking, jumping, climbing)
public class PlayerMovement : NetworkBehaviour {
    //Move amount fields
    [SerializeField] private float speedWalk;
    [SerializeField] private float speedSprint;
    [SerializeField] private float jumpForce;
    [SerializeField] private int jumpCountMax;

    //Check fields
    [SerializeField] private Transform feetPos;
    [SerializeField] private float checkRadius;

    //Jump helper fields
    private bool isJumping;
    private bool isGrounded;
    [SerializeField] private LayerMask whatIsGround;
    private int jumpCount = 0;

    //Time fields
    private float jumpTimeCounter;
    [SerializeField] private float jumpTime;

    //Stamina
    private float staminaLeft = 0;
    [SerializeField] private float staminaMax = 100;

    //Misc
    private Rigidbody rB;
    private FollowCamera cmF;
    [SerializeField] private GameObject model;
    private bool isServerRdy = false;
    private Animator anim;
    private bool hasPlayedJumpAnim = false;

    //Runs when the object is created
    private void Start() {
        cmF = Camera.main.GetComponent<FollowCamera>();

        //Makes sure that the client is ready and runs the coresponding code.
        StartCoroutine(WaitForReady());

        //Makes it so that only your own client runs the code below the if statement
        if (!hasAuthority) {
            return;
        }
    }

    //Use this to receive the message from the Server on the Client's side
    public void ReadyMessage(NetworkMessage networkMessage) {
        RunOnClientRdy();
    }

    void RunOnClientRdy() {
        //Gathers the rigidbody
        rB = GetComponent<Rigidbody>();

        //Gets the animator
        anim = GetComponentInChildren<Animator>();

        //Updates the camera to follow this object
        cmF.UpdateTarget(gameObject);

        //Object is now ready to work
        isServerRdy = true;
    }

    //Waits till the client is ready to use
    IEnumerator WaitForReady() {
        //Wait till it's ready
        while (!hasAuthority) {
            yield return new WaitForSeconds(0.25f);
        }

        //Run stuff that should run when the client is ready
        RunOnClientRdy();
    }

    //Runs at a fixed interval
    void FixedUpdate() {
        if (isServerRdy) {
            //Makes it so that only your own client runs the code below the if statement
            if (!hasAuthority) {
                return;
            }

            //Handle jumping
            JumpHandling();

            //Handle moving (x, z-coordinates)
            MoveHandling();
            
        }
    }

    //Takes care of moving in the x and z-axis (walk and sprint)
    void MoveHandling() {
        //Get vector that points in the direction of where the player should be headin (X, and Z axis)
        var normalizedVel = ((transform.forward * Input.GetAxis("Vertical")) + (transform.right * Input.GetAxis("Horizontal"))).normalized;

        float speedCurrent = speedWalk;

        if (staminaLeft>0 && Input.GetButton("Sprint")) {
            staminaLeft--;
            speedCurrent = speedSprint;
        } else if (!Input.GetButton("Sprint")&&(staminaLeft < staminaMax)) {
            staminaLeft++;
        }

        //Combines all the data in to one.
        rB.velocity = normalizedVel * speedCurrent + new Vector3(0, rB.velocity.y, 0);
        
        anim.SetBool("isWalking", Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0);
    }

    //Takes care of jumping with levitation feature (Mario like, the longer you hold space, the higher up you become.)
    void JumpHandling() {
        //Checks if the player is on the ground by checking a trigger sphere around an empty gameobject (location).
        var s = Physics.OverlapSphere(feetPos.position, checkRadius, whatIsGround);
        //print(s.Length);
        isGrounded = s.Length > 1;

        //In air and still holding down jump button
        if (isJumping && Input.GetButton("Jump")) {
            if (jumpTimeCounter > 0) {
                rB.velocity = Vector3.up * jumpForce;
                jumpTimeCounter -= Time.deltaTime;
            } else {
                isJumping = false;
            }
        }

        //Reset jump count if on ground
        if (isGrounded) {
            jumpCount = jumpCountMax;
        }

        //On ground or has more jumps left and pressed jump
        if ((isGrounded||((jumpCount>0) && !isJumping)) && Input.GetButtonDown("Jump")) {
            isJumping = true;
            jumpCount--;
            jumpTimeCounter = jumpTime;
            rB.velocity = Vector3.up * jumpForce;
            anim.Play("Jump");
        }

        //Released jump key
        if (Input.GetButtonUp("Jump")) {
            isJumping = false;
        }

        //if (isJumping && !hasPlayedJumpAnim) {
        //    anim.Play("Jump");
        //    hasPlayedJumpAnim = true;
        //} else if (!isJumping) {

        //}
    }
}