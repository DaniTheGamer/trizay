﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This is the script that runs when following a player. (Client only, obviously)
public class FollowCamera : MonoBehaviour {
    [SerializeField] private float radiusFromPlayer = 3;
    [SerializeField] private float rotateSpeed = 0.1f;
    [SerializeField] private float zoomSpeed = 1;
    private GameObject target;
    private GameObject UIMen;
    private bool isSmartboard = false;
    private bool enabledSmartboard = false;

    private Vector2 camAngles = new Vector2();
    private bool isPlayer = false;
    private bool enabledPlayer = false;

    private QAManager qAMan;

    public void SetIsSmartboard(bool val) {
        isSmartboard = val;
    }

    private void Start() {
        //#if UNITY_EDITOR
        //    if (Input.GetKeyDown(KeyCode.LeftAlt)) {
        //        if (Cursor.visible) {
        //            Cursor.visible = false;
        //            Cursor.lockState = CursorLockMode.Locked;
        //        } else {
        //            Cursor.visible = true;
        //            Cursor.lockState = CursorLockMode.None;
        //        }
        //    }
        //#else
        //    Cursor.visible = false;
        //    Cursor.lockState = CursorLockMode.Locked;
        //#endif
        
        qAMan = GameObject.Find("UI Cap").GetComponent<QAManager>();
        GameObject.Find("LobbyManager").transform.Find("Running server").gameObject.SetActive(false);
    }

    public void UpdateTarget(GameObject go) {
        target = go;
    }

    private void FixedUpdate() {
        if (isSmartboard && !enabledSmartboard) {
            UIMen.transform.Find("Question UI").gameObject.SetActive(true);
            enabledSmartboard = true;
        } else if (isPlayer && !enabledPlayer) {
            UIMen.transform.Find("Player POV").gameObject.SetActive(true);
            enabledPlayer = true;
        }

        if (UIMen == null) {
            UIMen = GameObject.Find("UI Cap");
        }
    }

    void LateUpdate() {
        if (Input.GetKeyDown(KeyCode.X)) {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        if (isPlayer) { //Is target a player
            camAngles.x += Input.GetAxis("Mouse X") * rotateSpeed;

            float verticalMouse = Input.GetAxis("Mouse Y") * rotateSpeed;
            if ((camAngles.y + 2*verticalMouse > 0 && verticalMouse < 0)|| (camAngles.y + verticalMouse < Mathf.PI && verticalMouse > 0)) { //Makes it so that we don't rotate around player, making it confusing for the player of what is up and what is down
                camAngles.y += verticalMouse;
            }

            radiusFromPlayer -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

            if (radiusFromPlayer > 7) {
                radiusFromPlayer = 7;
            } else if (radiusFromPlayer < 0.01) {
                radiusFromPlayer = 0.01f;
            }

            transform.position = GetPointOnSphere(target.transform.position, radiusFromPlayer, -camAngles.x, camAngles.y);
            transform.LookAt(target.transform.position);
            target.transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);
        } else if (isSmartboard) {
            var uIQ = UIMen.transform.Find("Question UI");
            var uIQuestion = uIQ.Find("Header").Find("Question");
            var uIQuestionText = uIQuestion.GetComponent<Text>();

            var cuQ = qAMan.GetCurrentQA();
            //Debug.LogError(qAMan);

            //Debug.LogError(cuQ);
            //Debug.LogError(uIQuestionText);
            //Debug.LogError(cuQ.Question);
            //Debug.LogError(uIQuestionText.text);

            uIQuestionText.text = cuQ.Question;

            var parentAnswerBoxes = uIQ.Find("4 Answers");

            parentAnswerBoxes.Find("Answer box 1").Find("Answer 1").GetComponent<Text>().text = cuQ.Answers[0].text;
            parentAnswerBoxes.Find("Answer box 2").Find("Answer 2").GetComponent<Text>().text = cuQ.Answers[1].text;
            parentAnswerBoxes.Find("Answer box 3").Find("Answer 3").GetComponent<Text>().text = cuQ.Answers[2].text;
            parentAnswerBoxes.Find("Answer box 4").Find("Answer 4").GetComponent<Text>().text = cuQ.Answers[3].text;

            //parentAnswerBoxes = uIQ.Find("3 Answers");

            //parentAnswerBoxes.Find("Answer 1").Find("Text").GetComponent<Text>().text = cuQ.Answers[0].text;
            //parentAnswerBoxes.Find("Answer 2").Find("Text").GetComponent<Text>().text = cuQ.Answers[1].text;
            //parentAnswerBoxes.Find("Answer 3").Find("Text").GetComponent<Text>().text = cuQ.Answers[2].text;

            //parentAnswerBoxes = uIQ.Find("2 Answers");

            //parentAnswerBoxes.Find("Answer 1").Find("Text").GetComponent<Text>().text = cuQ.Answers[0].text;
            //parentAnswerBoxes.Find("Answer 2").Find("Text").GetComponent<Text>().text = cuQ.Answers[1].text;
        } else if (target != null) {
            if (target.CompareTag("Player")) {
                isPlayer = true;
                GetComponent<Camera>().orthographic = false; //Perspective view
            } else if (target.GetComponent<UserSmartboard>() != null) {
                isSmartboard = true;
            }
        }
    }

    //Gets a point on a sphere, based on the center of the sphere, the radius and the angles from center to point
    Vector3 GetPointOnSphere(Vector3 center, float r, float degX, float degY) { //Deg x is radians arround y-axis, and deg y is radians arround x-axis
        float x = r * Mathf.Sin(degY) * Mathf.Cos(degX);
        float y = r * Mathf.Cos(degY);
        float z = r * Mathf.Sin(degY) * Mathf.Sin(degX);

        return new Vector3(x, y, z) + center;
    }
}