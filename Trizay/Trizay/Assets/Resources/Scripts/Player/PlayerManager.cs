﻿using UnityEngine;
using UnityEngine.Networking;

//Keeps track of data for player (like stamina, but not the score, since that is user related)
public class PlayerManager : NetworkBehaviour {
    [ClientRpc]
    public void RpcChangeName(string newName) {
        gameObject.name = newName;
    }

    [ClientRpc]
    public void RpcChangeColor(float r, float g, float b) {
        var rends = GetComponentsInChildren<Renderer>();
        foreach (var rend in rends) {
            rend.material.color = new Color(r, g, b, rend.material.color.a);
        }
    }
}